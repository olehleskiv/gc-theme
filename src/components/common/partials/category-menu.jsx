import React from 'react';
import { Link } from 'react-router-dom';

function CategoryMenu( props ) {
    const { type } = props;

    function onShowMenu( e ) {
        if ( !document.querySelector( '.category-dropdown.is-on' ).classList.contains( "show" ) ) {
            document.querySelector( '.category-dropdown.is-on' ).classList.add( "show" );
            document.querySelector( '.category-dropdown.is-on > .dropdown-menu' ).classList.add( "show" );
        } else {
            document.querySelector( '.category-dropdown.is-on' ).classList.remove( "show" );
            document.querySelector( '.category-dropdown.is-on > .dropdown-menu' ).classList.remove( "show" );
        }
        e.preventDefault();
    }

    return (
        <div className={ `dropdown category-dropdown ${type === 2 ? 'is-on' : ''}` }>
            <Link to="#" className="dropdown-toggle" onClick={ onShowMenu } role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories">
                Категории товаров
            </Link>

            <div className="dropdown-menu">
                <nav className="side-nav">
                    <ul className="menu-vertical sf-arrows sf-js-enabled">
                        <li className="megamenu-container">
                            <Link className="sf-with-ul" to="#">АВТОЗАПЧАСТИ</Link>

                            <div className="megamenu">
                                <div className="row no-gutters">
                                    <div className="col-md-8">
                                        <div className="menu-col">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="menu-title">Вентиляционные системы</div>
                                                    <ul>
                                                        <li><Link to="#">Вентиляторы печки</Link></li>
                                                        <li><Link to="#">Клапаны печки</Link></li>
                                                        <li><Link to="#">Корпуса фильтров кабины</Link></li>
                                                        <li><Link to="#">Моторчики печки</Link></li>
                                                        <li><Link to="#">Проводка печки</Link></li>
                                                        <li><Link to="#">Радиаторы печки</Link></li>
                                                        <li><Link to="#">Резисторы печки</Link></li>
                                                    </ul>

                                                    <div className="menu-title">Выхлопная система</div>
                                                    <ul>
                                                        <li><Link to="#">Выпускные коллекторы</Link></li>
                                                        <li><Link to="#">Выхлопные трубы</Link></li>
                                                        <li><Link to="#">Глушители</Link></li>
                                                        <li><Link to="#">Датчики давления выхлопных газов</Link></li>
                                                    </ul>
                                                </div>

                                                <div className="col-md-6">
                                                    <div className="menu-title">Двигатели и навесное</div>
                                                    <ul>
                                                        <li><Link to="#">Блок двигателя</Link></li>
                                                        <li><Link to="#">Вакуумные клапаны</Link></li>
                                                        <li><Link to="#">Вакуумные насосы</Link></li>
                                                        <li><Link to="#">Датчики детонации</Link></li>
                                                    </ul>

                                                    <div className="menu-title">Кузовные детали</div>
                                                    <ul>
                                                        <li><Link to="#">Балки</Link></li>
                                                        <li><Link to="#">Бамперы</Link></li>
                                                        <li><Link to="#">Двери</Link></li>
                                                        <li><Link to="#">Задние двери, крышки багажника</Link></li>
                                                        <li><Link to="#">Комплекты частей кузова</Link></li>
                                                        <li><Link to="#">Кабины</Link></li>
                                                        <li><Link to="#">Капоты</Link></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-4">
                                        <div className="banner banner-overlay">
                                            <Link to={ `${process.env.PUBLIC_URL}/shop/sidebar/list` } className="banner banner-menu">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/banner-1.jpg` } alt="Banner" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="megamenu-container">
                            <Link className="sf-with-ul" to="#">Автоаксессуары</Link>

                            <div className="megamenu">
                                <div className="row no-gutters">
                                    <div className="col-md-8">
                                        <div className="menu-col">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="menu-title">Автоаксессуары для тюнинга</div>
                                                    <ul>
                                                        <li><Link to="#">Автомобильная пленка</Link></li>
                                                        <li><Link to="#">Декоративные планки</Link></li>
                                                        <li><Link to="#">Защитные кожухи</Link></li>
                                                        <li><Link to="#">Зеркала, накладки</Link></li>
                                                        <li><Link to="#">Накладки на бампер</Link></li>
                                                    </ul>

                                                    <div className="menu-title">Багажники</div>
                                                    <ul>
                                                        <li><Link to="#">Боксы на крышу</Link></li>
                                                        <li><Link to="#">Сумки и чехлы</Link></li>
                                                        <li><Link to="#">Для перевозки лыж</Link></li>
                                                        <li><Link to="#">На крышу</Link></li>
                                                        <li><Link to="#">Сетки на крышу</Link></li>
                                                    </ul>

                                                </div>

                                                <div className="col-md-6">
                                                    <div className="menu-title">Коврики</div>
                                                    <ul>
                                                        <li><Link to="#">В багажник</Link></li>
                                                        <li><Link to="#">Велюровые</Link></li>
                                                        <li><Link to="#">Крепежные элементы</Link></li>
                                                        <li><Link to="#">Прочее</Link></li>
                                                        <li><Link to="#">Резиновые</Link></li>
                                                    </ul>

                                                    <div className="menu-title">Парковочные датчики и камеры заднего вида</div>
                                                    <ul>
                                                        <li><Link to="#">Камеры заднего вида</Link></li>
                                                        <li><Link to="#">Парковочные датчики</Link></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-4">
                                        <div className="banner banner-overlay">
                                            <Link to={ `${process.env.PUBLIC_URL}/shop/sidebar/list` } className="banner banner-menu">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/banner-2.jpg` } alt="Banner" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        {/* <li className="megamenu-container">
                            <Link className="sf-with-ul" to="#">Cooking</Link>

                            <div className="megamenu">
                                <div className="menu-col">
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="menu-title">Cookware</div>
                                            <ul>
                                                <li><Link to="#">Cookware Sets</Link></li>
                                                <li><Link to="#">Pans, Griddles &amp; Woks</Link></li>
                                                <li><Link to="#">Pots</Link></li>
                                                <li><Link to="#">Skillets &amp; Grill Pans</Link></li>
                                                <li><Link to="#">Kettles</Link></li>
                                                <li><Link to="#">Soup &amp; Stockpots</Link></li>
                                            </ul>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="menu-title">Dinnerware &amp; Tabletop</div>
                                            <ul>
                                                <li><Link to="#">Plates</Link></li>
                                                <li><Link to="#">Cups &amp; Mugs</Link></li>
                                                <li><Link to="#">Trays &amp; Platters</Link></li>
                                                <li><Link to="#">Coffee &amp; Tea Serving</Link></li>
                                                <li><Link to="#">Salt &amp; Pepper Shaker</Link></li>
                                            </ul>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="menu-title">Cooking Appliances</div>
                                            <ul>
                                                <li><Link to="#">Microwaves</Link></li>
                                                <li><Link to="#">Coffee Makers</Link></li>
                                                <li><Link to="#">Mixers &amp; Attachments</Link></li>
                                                <li><Link to="#">Slow Cookers</Link></li>
                                                <li><Link to="#">Air Fryers</Link></li>
                                                <li><Link to="#">Toasters &amp; Ovens</Link></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div className="row menu-banners">
                                        <div className="col-md-4">
                                            <div className="banner">
                                                <Link to="#">
                                                    <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/1.jpg` } alt="banner" />
                                                </Link>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="banner">
                                                <Link to="#">
                                                    <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/2.jpg` } alt="banner" />
                                                </Link>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="banner">
                                                <Link to="#">
                                                    <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/3.jpg` } alt="banner" />
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li> */}

                        {/* <li className="megamenu-container">
                            <Link className="sf-with-ul" to="#">Clothing</Link>

                            <div className="megamenu">
                                <div className="row no-gutters">
                                    <div className="col-md-8">
                                        <div className="menu-col">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="menu-title">Women</div>
                                                    <ul>
                                                        <li><Link to="#"><strong>New Arrivals</strong></Link></li>
                                                        <li><Link to="#"><strong>Best Sellers</strong></Link></li>
                                                        <li><Link to="#"><strong>Trending</strong></Link></li>
                                                        <li><Link to="#">Clothing</Link></li>
                                                        <li><Link to="#">Shoes</Link></li>
                                                        <li><Link to="#">Bags</Link></li>
                                                        <li><Link to="#">Accessories</Link></li>
                                                        <li><Link to="#">Jewlery &amp; Watches</Link></li>
                                                        <li><Link to="#"><strong>Sale</strong></Link></li>
                                                    </ul>
                                                </div>

                                                <div className="col-md-6">
                                                    <div className="menu-title">Men</div>
                                                    <ul>
                                                        <li><Link to="#"><strong>New Arrivals</strong></Link></li>
                                                        <li><Link to="#"><strong>Best Sellers</strong></Link></li>
                                                        <li><Link to="#"><strong>Trending</strong></Link></li>
                                                        <li><Link to="#">Clothing</Link></li>
                                                        <li><Link to="#">Shoes</Link></li>
                                                        <li><Link to="#">Bags</Link></li>
                                                        <li><Link to="#">Accessories</Link></li>
                                                        <li><Link to="#">Jewlery &amp; Watches</Link></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-4">
                                        <div className="banner banner-overlay">
                                            <Link to={ `${process.env.PUBLIC_URL}/shop/sidebar/list` } className="banner banner-menu">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/home/menu/banner-3.jpg` } alt="Banner" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>

                                <div className="menu-col menu-brands">
                                    <div className="row">
                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/1.png` } alt="Brand Name" />
                                            </Link>
                                        </div>

                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/2.png` } alt="Brand Name" />
                                            </Link>
                                        </div>

                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/3.png` } alt="Brand Name" />
                                            </Link>
                                        </div>

                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/4.png` } alt="Brand Name" />
                                            </Link>
                                        </div>

                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/5.png` } alt="Brand Name" />
                                            </Link>
                                        </div>

                                        <div className="col-lg-2">
                                            <Link to="#" className="brand">
                                                <img src={ `${process.env.PUBLIC_URL}/assets/images/brands/6.png` } alt="Brand Name" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li> */}
                        <li><Link to="#">МОТОЗАПЧАСТИ</Link></li>
                        <li><Link to="#">ШИНЫ</Link></li>
                    </ul>
                </nav>
            </div>
        </div>
    );
}

export default CategoryMenu;